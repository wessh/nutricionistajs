console.log("Arquivo principal.js, carregado com sucesso!");

var mod = document.querySelector(".titulo");

mod.textContent = "Aparecida Nutricionista"

///
/// Calculando o IMC
///

//Captura de dados. 
var paciente = document.querySelector("#paciente");

var tdPeso = paciente.querySelector(".info-peso").textContent;
//var peso  = tdPeso.textContent;

var tdAltura = paciente.querySelector(".info-altura").textContent;
//var altura = tdAltura.textContent;

console.log("Paciente: " + paciente.querySelector(".info-nome").textContent + "\n" +
    "Peso: " + tdPeso + "\n" + "Altura: " + tdAltura);

var imc = tdPeso / (tdAltura * tdAltura);

console.log("Seu imc é de " + imc);

var tdIMC = document.querySelector("#paciente");
var linhaIMC = tdIMC.querySelector(".info-imc");
linhaIMC.textContent = imc;